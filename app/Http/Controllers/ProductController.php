<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Helpers;

class ProductController extends Controller
{
  /**
   * Returns products belonging to user
   *
   * @return \Illuminate\Http\JsonResponse
   */
    public function index() {
      $products = auth()->user()->products;

      if(function_exists('carbon')) {
        // echo 'Hello';
        $time = carbon();
        $toupper = strupper('maninder');
        // dd($time);
        // dd($toupper);
      }

      return response()->json([
         'success' => true,
         'data' => $products
      ]);
    }

    public function show($id)
    {
        $product = auth()->user()->products()->find($id);

        if (!$product) {
            return response()->json([
                'success' => false,
                'message' => 'Product with id ' . $id . ' not found'
            ], 400);
        }

        return response()->json([
            'success' => true,
            'data' => $product->toArray()
        ], 400);
    }

    /**
   * Handles Product Save Request
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required|integer'
        ]);

        $product = new Product();
        $product->name = $request->name;
        $product->price = $request->price;

        if (auth()->user()->products()->save($product)) {
          return response()->json([
              'success' => true,
              'data' => $product->toArray()
          ]);
        } else {
          return response()->json([
              'success' => false,
              'message' => 'Product could not be added'
          ], 500);
        }
    }

    /**
   * Handles Product Save Request
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
    public function update(Request $request, $id)
    {
        $product = auth()->user()->products()->find($id);

        if (!$product) {
            return response()->json([
                'success' => false,
                'message' => 'Product with id ' . $id . ' not found'
            ], 400);
        }

        $updated = $product->fill($request->all())->save();

        if ($updated)
            return response()->json([
                'success' => true
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Product could not be updated'
            ], 500);
    }

    public function destroy($id)
    {
        $product = auth()->user()->products()->find($id);

        if (!$product) {
            return response()->json([
                'success' => false,
                'message' => 'Product with id ' . $id . ' not found'
            ], 400);
        }

        if ($product->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Product could not be deleted'
            ], 500);
        }
    }
}
